# SitT Sketches

The SitT Sketchbook, with Sketches by NCR for The SunShining

**Why?**

Because I create a lot of sketches for The SunShining. I tried sharing them on shutterstock, but no views.
So I was like why not create this git repo and website. This is a place for me to organize and view my sketchs, as well as share them.
So hi, if you are a random person viewing this!


---

**Art Style**

As anybody knows you do somthing over time you change how you do it. When I first got started for certain sketches that I wanted colored, I actually used color. I have this new method now where I all I use is white, then I alpah mask a white layor with a color layer if I want color. I beileve this method to be genious.

Also I try to remain consistent with the brush and brush size, but there are a few where I use different brushes.

All that to say that this repo contains large tiff files with all the layers and can be opened in sketchbook. Sketchbook is that one beatiful and simple app that I can't seem to replace.


**How to change color**

If you have Skechbook pro, not sure if certain features are limited in the free version, you can alpha mask layers and merge layers. So even my older color sketches can be color changed. On my newer sketches, I have white sketch layers and color layers.

**Style Update**

White is really hard to see, so I am going to make black my deafult color.

**How to view my sketchbook**

You can preview some of the images on the webpage(Only veiwable in Safari). But I would recommend downloading this entire folder and previewing this in your file exporer or finder. There are also good apps for previewing photos on your device. But also remember that these files can be opened in Sketckbook where you can view all the layers.

Phototagger - Availble for Mac OS and Windows, I just discovered this app. Beutiful and free for viewing. But you have to pay to tag.

Picturama - Free way to view and tag photos. I might use this to add tags to some of my sketches.